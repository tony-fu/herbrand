# Herbrand Logic Unification

## Terms

A term can be 
- A variable `HVar "X"`
- A constant `HConst "a"`
- A binary function `HFunction term1 term2` where term1 and term2 are terms
```haskell
data HTerm = HConst String | HFunction HTerm HTerm | HVar String
```

## Unification
Unification will add a constraint to the store.
There are six cases for unifaction. Examples are given below.

- Const and Const
```haskell
(HConst "a") `unify` (HConst "b") 
-- => False
(HConst "a") `unify` (HConst "a") 
-- => True
```

- Const and Variable
```haskell
-- 
(HVar "X") `unify` (HConst "a") 
-- => True {x = a}
(HVar "X") `unify` (HConst "b") 
-- => False
```

- Variable and Variable
```haskell
-- 
(HVar "X") `unify` (HVar "X") 
-- => True, tautology
(HVar "X") `unify` (HVar "Y") 
-- => True, {x = y}
(HVar "Y") `unify` (HVar "Z") 
-- => True {x = y, y = z}
(HVar "X") `unify` (HConst "a") 
-- => True {x = a, y = a, z = a}
(HVar "Y") `unify` (HConst "b") 
-- => False
```

- Const and Function
```haskell
-- 
(HConst "a") `unify` (HFunction term1 term2) 
-- => False
```

- Function and Function
```haskell
-- 
(HConst (HVar "X") (HVar "Y")) `unify` (HFunction (HConst "a") (HConst "b") ) 
-- => True {x = a, y = b}
```

- Function and Variable
```haskell
-- 
(HVar "X") `unify` (HFunction (HVar "X") (HConst "b") ) 
-- => False, x occured as a function argument
(HVar "X") `unify` (HFunction (HVar "Y") (HConst "b") ) 
-- => True {x = f(y, b)}
```

## Entailment
Entailment will not add a rule in store. If the constraint cannot be entailed by the current store, `entail` will fail.
There are six cases for entailment. Examples are given below.

- Const and Const
```haskell
(HConst "a") `entail` (HConst "b") 
-- => False
(HConst "a") `entail` (HConst "a") 
-- => True
```

- Const and Variable
```haskell
-- 
(HVar "X") `entail` (HConst "a") 
-- => False
```

- Variable and Variable
```haskell
-- 
(HVar "X") `entail` (HVar "X") 
-- => True, tautology
(HVar "X") `entail` (HVar "Y") 
-- => False, unbounded variables can not be entailed
(HVar "X") `unify` (HConst "a")  -- unification is used here for demo purpose
-- => True {x = a}
(HVar "Y") `unify` (HConst "a")  -- unification is used here for demo purpose
-- => True {x = a, Y = a}
(HVar "X") `entail` (HVar "Y") 
-- => True
```

- Const and Function
```haskell
-- 
(HConst "a") `entail` (HFunction term1 term2) 
-- => False
```

- Function and Function
```haskell
-- 
(HConst (HVar "X") (HVar "Y")) `entail` (HFunction (HConst "a") (HConst "b") ) 
-- => False
```

- Function and Variable
```haskell
-- 
(HVar "X") `entail` (HFunction (HVar "X") (HConst "b") ) 
-- => False
(HVar "X") `entail` (HFunction (HVar "Y") (HConst "b") ) 
-- => False
```

## Checkpoint
Create a checkpoint of current store and rewind to it in a future point.
```haskell
test = do
  r1 <- (HVar "X") `unify` (HConst "a")
  checkpoint <- createCheckPoint
  r2 <- (HVar "X") `unify` (HVar "Y")
  r3 <- (HVar "Z") `unify` (HVar "Y")
  r4 <- (HVar "Z") `unify` (HConst "b")
  rewindToCheckPoint checkpoint
  return (r1, r2, r3, r4)

-- Steps: (True,True,True,False)
-- Store {x = a}
```