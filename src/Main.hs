module Main where

import Control.Monad.State.Lazy
import Data.List (find)
import Data.Maybe (isNothing)
import Data.Set (Set)
import qualified Data.Set as Set
import Prelude hiding (empty, lookup)

data HTerm = HConst String | HFunction HTerm HTerm | HVar String

instance Show HTerm where
  show (HConst s) = s
  show (HFunction t1 t2) = (show t1) ++ " -> " ++ (show t2)
  show (HVar s) = "Var(" ++ s ++ ")"

isVar :: HTerm -> Bool
isVar (HVar _) = True
isVar _ = False

isConst :: HTerm -> Bool
isConst (HConst _) = True
isConst _ = False

isFunction :: HTerm -> Bool
isFunction (HFunction _ _) = True
isFunction _ = False

allFunctionArgs :: HTerm -> (Set String)
allFunctionArgs term = go term Set.empty
  where
    go :: HTerm -> Set String -> Set String
    go (HConst _) set' = set'
    go (HVar v) set' = v `Set.insert` set'
    go (HFunction t1 t2) set' = (go t1 set') `Set.union` (go t2 set')

varOccured :: String -> HTerm -> StateT [(Set String, Maybe HTerm)] IO Bool
varOccured v term = do
  store <- get
  let allArgs = allFunctionArgs term
      occured =
        foldr
          ( \bucket z ->
              if v `Set.member` (fst bucket)
                then not (allArgs `Set.disjoint` (fst bucket))
                else z
          )
          False
          store
  return occured

bindVarToTerm :: String -> HTerm -> StateT [(Set String, Maybe HTerm)] IO ()
bindVarToTerm v term = do
  store <- get
  let newStore =
        fmap
          ( \bucket ->
              if v `Set.member` (fst bucket)
                then ((fst bucket), Just term)
                else bucket
          )
          store
  put newStore

varIsBoundTo :: String -> StateT [(Set String, Maybe HTerm)] IO (Maybe HTerm)
varIsBoundTo v = do
  store <- get
  let isBoundTo =
        foldr
          ( \bucket z ->
              if not (isNothing z)
                then z
                else
                  if v `Set.member` (fst bucket)
                    then snd bucket
                    else Nothing
          )
          Nothing
          store
  return isBoundTo

varInStore :: String -> StateT [(Set String, Maybe HTerm)] IO Bool
varInStore v = do
  store <- get
  let result =
        foldr
          ( \st z ->
              let varSet = fst st
                  r = (v `Set.member` varSet) || z
               in r
          )
          False
          store
  return result

varsInSameBucket :: String -> String -> StateT [(Set String, Maybe HTerm)] IO Bool
varsInSameBucket v1 v2 = do
  store <- get
  let inSameBucket =
        foldr
          ( \bucket z ->
              if v1 `Set.member` (fst bucket)
                then v2 `Set.member` (fst bucket) || z
                else z
          )
          False
          store
  return inSameBucket

putNewVarInStore :: String -> StateT [(Set String, Maybe HTerm)] IO ()
putNewVarInStore v = do
  store <- get
  put $ ((Set.singleton v), Nothing) : store
  return ()

putVarInSameBucket :: String -> String -> StateT [(Set String, Maybe HTerm)] IO ()
putVarInSameBucket v1 v2 = do
  store <- get
  let newStore =
        fmap
          ( \bucket ->
              if v1 `Set.member` (fst bucket)
                then (v2 `Set.insert` (fst bucket), snd bucket)
                else bucket
          )
          store
  put newStore

unionTwoBuckets :: String -> String -> StateT [(Set String, Maybe HTerm)] IO ()
unionTwoBuckets v1 v2 = do
  store <- get
  let bucket1 = find (\bucket -> v1 `Set.member` (fst bucket)) store
      bucket2 = find (\bucket -> v2 `Set.member` (fst bucket)) store
  case (bucket1, bucket2) of
    (Just b1, Just b2) -> do
      let unioned = (fst b1) `Set.union` (fst b2)
          newBucket = (unioned, Nothing)
          removedBucket1 = dropWhile (\bucket -> v1 `Set.member` (fst bucket)) store
          removedBucket1and2 = dropWhile (\bucket -> v2 `Set.member` (fst bucket)) removedBucket1
      put (newBucket : removedBucket1and2)
    _ -> return ()

-- Unification
unify :: HTerm -> HTerm -> StateT [(Set String, Maybe HTerm)] IO Bool
-- const =:= const
unify (HConst a) (HConst b) = do
  return (a == b)

-- var =:= const
unify (HVar v) cnst@(HConst _) = do
  varExist <- varInStore v
  case varExist of
    False -> do
      putNewVarInStore v
      bindVarToTerm v cnst
      return True
    True -> do
      varBoundTo <- varIsBoundTo v
      case varBoundTo of
        Nothing -> do
          bindVarToTerm v cnst
          return True
        (Just term) -> unify cnst term

-- const =:= var
unify cnst@(HConst _) (HVar v) = do
  varExist <- varInStore v
  case varExist of
    False -> do
      putNewVarInStore v
      bindVarToTerm v cnst
      return True
    True -> do
      varBoundTo <- varIsBoundTo v
      case varBoundTo of
        Nothing -> do
          bindVarToTerm v cnst
          return True
        (Just term) -> unify cnst term

-- var =:= var
unify (HVar v1) (HVar v2) = do
  v1InStore <- varInStore v1
  v2InStore <- varInStore v2
  case (v1InStore, v2InStore) of
    (False, False) -> do
      putNewVarInStore v1
      putVarInSameBucket v1 v2
      return True
    (True, False) -> do
      putVarInSameBucket v1 v2
      return True
    (False, True) -> do
      putVarInSameBucket v2 v1
      return True
    (True, True) -> do
      sameBucket <- varsInSameBucket v1 v2
      case sameBucket of
        True -> return True
        False -> do
          v1BoundTo <- varIsBoundTo v1
          v2BoundTo <- varIsBoundTo v2
          case (v1BoundTo, v2BoundTo) of
            (Nothing, Nothing) -> do
              -- join two buckets
              unionTwoBuckets v1 v2
              return True
            (Nothing, Just term) -> do
              -- join two buckets
              unionTwoBuckets v1 v2
              bindVarToTerm v1 term
              return True
            (Just term, Nothing) -> do
              -- join two buckets
              unionTwoBuckets v1 v2
              bindVarToTerm v1 term
              return True
            (Just term1, Just term2) -> do
              -- if term1 == term2 then join; else fail
              result <- unify term1 term2
              case result of
                False -> return False
                True -> do
                  unionTwoBuckets v1 v2
                  bindVarToTerm v1 term1
                  return True

-- var =:= fun
unify (HVar v) fun@(HFunction _ _) = do
  varIsInStore <- varInStore v
  occured <- varOccured v fun
  case occured of
    True -> return False
    False -> do
      case varIsInStore of
        True -> do
          boundTo <- varIsBoundTo v
          case boundTo of
            Nothing -> return False
            Just term -> unify fun term
        False -> do
          putNewVarInStore v
          bindVarToTerm v fun
          return True

-- fun =:= var
unify fun@(HFunction _ _) (HVar v) = do
  varIsInStore <- varInStore v
  occured <- varOccured v fun
  case occured of
    True -> return False
    False -> do
      case varIsInStore of
        True -> do
          boundTo <- varIsBoundTo v
          case boundTo of
            Nothing -> return False
            Just term -> unify fun term
        False -> do
          putNewVarInStore v
          bindVarToTerm v fun
          return True

-- fun =:= var
unify (HFunction _ _) (HConst _) = return False
-- var =:= fun
unify (HConst _) (HFunction _ _) = return False
-- fun =:= fun
unify (HFunction a b) (HFunction a' b') = do
  resultA <- unify a a'
  resultB <- unify b b'
  return (resultA && resultB)

-- Entailment
entail :: HTerm -> HTerm -> StateT [(Set String, Maybe HTerm)] IO Bool
-- const =:= const
entail (HConst a) (HConst b) = do
  return (a == b)

-- var =:= const
entail (HVar v) cnst@(HConst _) = do
  varExist <- varInStore v
  case varExist of
    False -> do
      return False
    True -> do
      varBoundTo <- varIsBoundTo v
      case varBoundTo of
        Nothing -> do
          return False
        (Just term) -> entail cnst term

-- const =:= var
entail cnst@(HConst _) (HVar v) = do
  varExist <- varInStore v
  case varExist of
    False -> do
      return False
    True -> do
      varBoundTo <- varIsBoundTo v
      case varBoundTo of
        Nothing -> do
          return False
        (Just term) -> entail cnst term

-- var =:= var
entail (HVar v1) (HVar v2) = do
  v1InStore <- varInStore v1
  v2InStore <- varInStore v2
  case (v1InStore, v2InStore) of
    (False, False) -> return False
    (True, False) -> return False
    (False, True) -> return False
    (True, True) -> do
      sameBucket <- varsInSameBucket v1 v2
      case sameBucket of
        True -> return True
        False -> return False

-- var =:= fun
entail (HVar v) fun@(HFunction _ _) = do
  varIsInStore <- varInStore v
  occured <- varOccured v fun
  case occured of
    True -> return False
    False -> do
      case varIsInStore of
        True -> do
          boundTo <- varIsBoundTo v
          case boundTo of
            Nothing -> return False
            Just term -> entail fun term
        False -> return False

-- fyn =:= var
entail fun@(HFunction _ _) (HVar v) = do
  varIsInStore <- varInStore v
  occured <- varOccured v fun
  case occured of
    True -> return False
    False -> do
      case varIsInStore of
        True -> do
          boundTo <- varIsBoundTo v
          case boundTo of
            Nothing -> return False
            Just term -> entail fun term
        False -> return False

-- fun =:= var
entail (HFunction _ _) (HConst _) = return False
-- var =:= fun
entail (HConst _) (HFunction _ _) = return False
-- fun =:= fun
entail (HFunction a b) (HFunction a' b') = do
  resultA <- entail a a'
  resultB <- entail b b'
  return (resultA && resultB)

-- CheckPoint
createCheckPoint :: StateT [(Set String, Maybe HTerm)] IO [(Set String, Maybe HTerm)]
createCheckPoint = do
  store <- get
  return store

rewindToCheckPoint :: [(Set String, Maybe HTerm)] ->  StateT [(Set String, Maybe HTerm)] IO ()
rewindToCheckPoint store = do
  put store


test = do
  r1 <- (HVar "X") `unify` (HConst "a")
  checkpoint <- createCheckPoint
  r2 <- (HVar "X") `unify` (HVar "Y")
  r3 <- (HVar "Z") `unify` (HVar "Y")
  r4 <- (HVar "Z") `unify` (HConst "b")
  rewindToCheckPoint checkpoint
  state <- get
  lift $ putStrLn "Steps:"
  lift $ print (r1, r2, r3, r4)
  lift $ putStrLn "Store:"
  lift $ print state

main = fmap fst $ (runStateT test) []

